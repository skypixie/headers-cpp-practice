#include <iostream>
#include "Helpers.h"


int main() {
	double a = 2;
	double b = 3;

	std::cout << "Squared sum of " << a << " and " << b <<
		" is " << SquareOfSum(a, b) << std::endl;
}